<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'warhamhorseshoes');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '/5dzT!O[*11MEP}&m4KD.V[dlALqD1jw[YZQ{i`#)A@%h-}kL>0x6+ZqE=m-IvEg');
define('SECURE_AUTH_KEY',  'fWad_7,J=wWN)x;W~>nc62H1JE,TV!ewP_/RPD5gwAs7giyy<L;09P.:>fY{G@mB');
define('LOGGED_IN_KEY',    'QqNkd`E*F_0r,Wi;.-p%Qq`j/VxPs&f8NkB=8D(a2c1-n_Esw[nNCriuwrlHw7y=');
define('NONCE_KEY',        '~&($X]/%VO (hw%@c|{r4<lG1?QmZS>~nm)=*,J@G#DFD&S%cue|feb46wOCWW0}');
define('AUTH_SALT',        'BV%bizyoFm+1Yf`|&_Sjpo$7l8@I[N{<X_ANay9BDJgI0r h]0{Y<M;m!Zf^Fw;6');
define('SECURE_AUTH_SALT', '?0s%I!}-&C2>)HHyW7>U5ykm+uM%BoCTOGA<LZu,*g!Lu(|BL71iXX@Gz/:{Ue2R');
define('LOGGED_IN_SALT',   '!]Dn6C2+tqvub1LWat_7rqZ7P,6QZU5*ZKt[15t(8Q&:dWlH&8v>AQgB-1?]{|DW');
define('NONCE_SALT',       'xeNRlU9PDzVnddf_4IlOln4,3bb6rlb,r$BX!WYF_;%%Lnj *e1kCHun-IqyGmAY');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
