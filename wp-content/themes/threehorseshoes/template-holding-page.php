<?php
/*
Template Name: Holding Page
*/
?>

<?php get_header(); ?>
      
  <div class="container">
    <header>
      <h1>Closed for some <br>love and attention</h1>
      <h2>- Re-opening Summer 2017 -</h2>
    </header>

    <section class="logo">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/images/3hs-logo.png" width="100%" height="auto">
    </section>

    <section class="base-content">
      <p>Sign up below if you'd like to keep up to date on our progress.</p>
      <section class="sign-up">
        <?php echo do_shortcode("[contact-form-7 id='18' title='Sign up']"); ?>
        <!-- <?php if (function_exists('wp_email_capture_form')) { wp_email_capture_form(); } ?> -->
        <!-- <input type="text" placeholder="Email Address">
        <input type="submit" value="Sign Up"></input> -->
      </section>
      <p>New website also under construction and coming soon.</p>
    </section>

    <footer>
      <p>69 The St, Wells-next-the-Sea NR23 1NL  |  <a href="mailto:info@warhamhorseshoes.co.uk">info@warhamhorseshoes.co.uk</a></p>
    </footer>
  </div>
<!-- 
<?php get_footer(); ?>
