<?php
/*
Template Name: Thank You
*/
?>

<?php get_header(); ?>
      
  <div class="container">
    <section class="logo">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/images/3hs-logo.png" width="100%" height="auto">
    </section>

    <section class="base-content">
      <h1>Thank You</h1>
      <h2>We we keep you up to date with our progress, and of course let you know as soon as our doors are open!</h2>
    </section>

    <footer>
      <p>69 The St, Wells-next-the-Sea NR23 1NL  |  <a href="mailto:info@warhamhorseshoes.co.uk">info@warhamhorseshoes.co.uk</a></p>
    </footer>
  </div>
<!-- 
<?php get_footer(); ?>
